import { page, z } from './2ombular';
import './style.css';

import RSA from './rsa';

const pages = {}
let page_;
function register(route, fn) {
    pages[route] = fn;
    if (page.route === route) {
        page_ = fn();
        page.update();
    }
}

const id = [...Array(12)].map(i=>(~~(Math.random()*16)).toString(36)).join('');
const ws = new WebSocket(`ws://${location.href.split('//')[1]}ws`);
ws.onmessage = listen;
ws.onclose = page.update;
ws.onopen = _ => send('@all', 'connected');

const keys = {};
const message_queue = {};
const messages = [];

function encrypt(data, encryptor) {
    let result = '';
    for (let i = 0; i < data.length; i += 53)
        result += encryptor.encrypt(data.slice(i, i+53));
    return result;
}

function decrypt(data, decryptor) {
    let result = '';
    // Чисто изза хекса длина x2
    for (let i = 0; i < data.length; i += 128) {
        let plain = decryptor.decrypt(data.slice(i, i+128));
        if (plain == null) throw new Error('Cannot decrypt message')
        result += plain;
    }
    return result;
}

const cmds = {
    message(from, to, text) {
        if (to === id) text = decrypt(text, keys[from].decryptor);
        messages.push({ from, to, text, time: new Date() });
    },
    connected(from) {
        messages.push({ from, text: 'is now connected', time: new Date() });
    },
    start_handshake(from, to, pub) {
        if (to === '@all' || keys[from] != undefined) return;
        const decryptor = new RSA();
        decryptor.generate(512, '10001');
        const encryptor = new RSA();
        encryptor.parsePublicPEM(pub);
        keys[from] = {encryptor, decryptor};
        send(from, 'end_handshake', decryptor.publicPEM());
    },
    end_handshake(from, to, pub) {
        if (to === '@all' || keys[from] == undefined) return;
        const encryptor = new RSA();
        encryptor.parsePublicPEM(pub);
        keys[from].encryptor = encryptor;
        if (message_queue[from])
            while (message_queue[from].length)
                send(from, 'message', encrypt(message_queue[from].shift(), encryptor));
    }
}

function send(to, cmd, ...args) {
    const data = JSON.stringify([id, to, cmd, ...args]);
    ws.send(data);
}

function listen(e) {
    try {
        const [from, to, cmd, ...args] = JSON.parse(e.data);
        if (to === id || to === '@all') cmds[cmd](from, to, ...args);
        page.update();
    } catch(e) {
        console.error(e)
    }
}

const ClientName = name => [z._span.cp.h1({
    onclick(e) { input_.value = `@${name} ${input_.value}`; }
}, name), ': '];

function LightenDarkenColor(col) {
    const num = parseInt(col, 16);
    let r = (num >> 16), g = ((num >> 8) & 0x00FF), b = (num & 0x0000FF);
    const amt = 200 - Math.min(r, g, b);
    r += amt; g += amt; b += amt;
    return (Math.min(b, 255) | (Math.min(g, 255) << 8) | (Math.min(r, 255) << 16)).toString(16);
}

const ColorMessage = (color, name, text) =>
    z.c0({ style: 'background: #' + LightenDarkenColor(color) }, name, text);

const Message = ({from, text, to}) =>
    to == undefined
        ? z.c1(ClientName(from), text)
    : to == '@all'
        ? z.c0(ClientName(from === id ? 'you' : from), text)
    : from == id
        ? ColorMessage(to.slice(0, 6), 'you: ', text)
    : ColorMessage(from.slice(0, 6), ClientName(from), text)

const Messages = _ => messages.map((_, i, arr) => Message(arr[arr.length - 1 - i]));

function send_message() {
    if (input_.value == '') return;
    let text = input_.value, to = '@all';
    if (text[0] == '@') {
        const i = text.indexOf(' ');
        const to_ = text.slice(1, i);
        if (/[0-9a-f]{12}/g.test(to_)) {
            to = to_;
            text = text.slice(i + 1);
        }
    }
    if (to == '@all') {
        send(to, 'message', text);
    } else {
        if (to in keys && keys[to].encryptor) {
            send(to, 'message', encrypt(text, keys[to].encryptor));
        } else {
            if (message_queue[to] == undefined)
                message_queue[to] = [];
            message_queue[to].push(text);
            if (keys[to] == undefined) {
                const decryptor = new RSA();
                decryptor.generate(512, '10001');
                keys[to] = { decryptor }
                send(to, 'start_handshake', decryptor.publicPEM());
            }
        }
    }
    
    messages.push({ from: id, to, text, time: new Date() });
    page.update();
    input_.value = '';
}

let input_;
const Input = z._input.fw({
    on$created(e) { input_ = e.target; },
    onkeydown(e) { if (e.keyCode == 13) send_message(); }
});

const Button = z._button({
    onclick(e) { send_message(); }
}, 'Send');

register('', _=> z.g.col.fh.main(
    z.s1.g.colr.ovh(Messages),
    z.g(z.s1(Input), Button)
));

const ConnectionCheck = _ =>
    ws.readyState == ws.CLOSED
        ? 'You have disconnected, what a pitty'
        : page_;

const Body = z.screen(ConnectionCheck);
page.setBody(Body);
