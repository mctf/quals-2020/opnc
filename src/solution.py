#!/home/blackmius/projects/mctf2020/opnc/env/bin/python3
import asyncio
import websockets
import json
import rsa
import logging
import binascii

url = "ws://opnc.mctf.online/ws"

def encrypt(data, key):
    res = b''
    data = data.encode()
    for i in range(0, len(data), 53):
        res += rsa.encrypt(data[i:i+53], key)
    return binascii.hexlify(res).decode()

def decrypt(data, key):
    res = b''
    data = binascii.unhexlify(data)
    for i in range(0, len(data), 64):
        res += rsa.decrypt(data[i:i+64], key)
    return res.decode()

publicKey, private = rsa.newkeys(512)
public = publicKey.save_pkcs1().decode()

keys = {}

async def main():
    async with websockets.connect(url) as websocket:
        def send(*args):
            asyncio.ensure_future(websocket.send(json.dumps(args)))
        async for message in websocket:
            try:
                sender, recipient, cmd, *args = json.loads(message)
                if cmd == 'start_handshake':
                    keys[sender] = rsa.PublicKey.load_pkcs1(args[0])
                elif cmd == 'end_handshake':
                    keys[sender] = rsa.PublicKey.load_pkcs1(args[0])
                    send(recipient, sender, 'end_handshake', public)
                    send(sender, recipient, 'end_handshake', public)
                elif cmd == 'message' and sender in keys and recipient in keys:
                    try:
                        msg = decrypt(args[0], private)
                        print(msg)
                        data = encrypt(msg, keys[recipient])
                        send(sender, recipient, 'message', data)
                    except Exception as e:
                        logging.error(e)
                    
            except Exception as e:
                logging.error(e)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
